# CHAMELEON_noblend

This is the code for the online version of the CHAMELEON melodic harmonisation assistant that does not include blending.

In this version several things are not working properly and several things are missing, but they will all be worked out
gradually.

v0.01 - 09 Oct 2018: no grouping.
v0.02 - 06 Oct 2018: grouping added.